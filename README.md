# 摩卡应用框架及解决方案

## 介绍

摩卡应用框架经历过 arena -> thunk -> saga 以及最近刚发布的 moka，moka 的核心是其实是 [dva@2](https://github.com/sorrycc/blog/issues/48)（准确的说，是使用 react-router 3版本的dva@2），只不过它在 dva@2 的基础之上提供了一层比较薄的封装和抽象。 dva 在业界比较受认可，它能够给我们的开发工作带来便利性，提高开发效率。moka 则基于 dva 封装，遵循开关原则，完全延续了原有接口，并结合我们的一些规范和约定，扩展了一些新的接口；除此之外， moka还定义了一些内部依赖，并且将原有的构建代码集中到提供的构建工具中，集中维护。

## 概念 & 规范

- **route 路由**, moka 内部使用的路由其实就是基于 [dva-react-router-3](https://github.com/dvajs/dva/tree/master/packages/dva-react-router-3)，知道一下就好。在我们的应用代码中，route 通常指的是路由路径，它会对应一个（默认或动态加载的）模块；从概念上来讲，我们的路由是跟着模块的概念走的，一个模块有一个路由，模块有子模块，子模块里面的路由其实就是子路由。
- **模块**，模块是我们新版的摩卡应用中一个约定的概念，规范它包含的要素如下：
	- `route`， 模块路由，通常在外部使用模块时进行关联
	- `component`，模块组件，关联模块的入口页面
	- `model`，模块模型，单数，模块的model定义
	- `models`，模块模型，复数，模块的model定义，支持多个model绑定
	- `index`，模块入口，通常在模块目录的index.js中聚合模块的各个内容要素

	> 在实际开发工作中，推荐使用IDE或编辑器的模板工具，快速创建模板，提高效率
- **model 模型**，这是dva中的概念，旨在以Model First的理念，集中处理数据模型和逻辑，将state，action，reducers这些概念统一在一个地方集中维护，并简化接口和调用。按照约定，它包含的要素如下：
	- `namespace`，命名空间，一个model的标识，同时作为action type的默认前缀，需要注意的是，在同一个namespace下dispatch一个action时，不必带上namespace前缀。命名上，namespace一般建议与模块名称及文件夹名称保持一致即可
	- `state`，初始state状态，对应原来我们讲的`initialState`
	- `subscriptions`，dva订阅对象，用法参考 [这里](https://github.com/dvajs/dva/issues/174)
	- `effects`，基于redux-saga定义，带副作用的action，每一个effects里面定义的action已经被自动使用 `takeEvery` 注册，所以不需要手动写这部分代码。这里命名没有特别的要求，使用camel命名即可，但是 payload 和 effects 对象参考示例写法
	- `reducers`，精简的reducer定义，以 namespace/${function name key} 作为匹配的action type，出入state和action，返回新的state


## moka 内置模块
- **moka**，核心对象，负责应用程序配置和初始化
  
  `import moka from '@lattebank/moka';`
- **$http**，http请求对象，和我们原来的一样的

  `import { $http } from '@lattebank/moka';`
- **connect**，react-redux接口，负责组装`mapStateToProps`的HOC装饰器
  
  `import { connect } from '@lattebank/moka';`
- **路由系列对象**，对应 `react-router v3`里面的内置对象

  `import { Link, ... } from '@lattebank/moka/router';`
- **routerRedux**，对应`react-router-redux`里面的内置对象

  `import { routerRedux } from '@lattebank/moka/router';`
  
## 使用指南
1. 安装或升级到最新版的 seed-cli
2. `seed-cli init webadmin#moka`
3. `npm install`
4. 编写具体业务模块（可以通过模板工具创建）
	1. 创建业务目录 `mkdir <bussiness module name>`
	2. 在业务目录下编写 service 接口代码 `touch service.js`
	3. 在业务目录下编写 model 模型代码  `touch model.js`
	4. 在业务目录下编写具体页面 `touch ThisIsJsxComponent.jsx`
	5. 在业务目录编写模块入口代码 `touch index.js`
	6. 在 rsrc/src/index.js 初始文件中定义路由并引入模块
	7. 构建 `npm run dev`
5. 以上是本地开发步骤，如果是初次运行，还需要运行dll处理
	1. 运行 `npm run dll`
	2. dll生成的文件在 rsrc/js 目录下，将新文件名更新到 src/main/webapp/WEB-INF/ftl/*.ftl 中
	3. 提交代码。 此次提交之后，如果 package.json 中的 DLL 未更改，则不需要再进行dll处理操作

## API 文档
#### app = moka(opts)
创建dva对象，并返回自身实例，以方便链式调用。`opts`包含：

- `defaultRoute`，默认路由，对应 `IndexRedirext`
- `defaultModule`，默认模块，默认加载页面并注册model，而非动态加载
- `...`，其他参数完全兼容dva，参照 [这里](https://github.com/dvajs/dva/blob/master/docs/API.md#app--dvaopts) 

#### app = moka.setup(setupProviders)
此接口为dva无关的接口，主要作用是包装应用程序初始化的一些行为，返回自身。默认情况下，它会加载 `httpProvider`（负责http拦截器行为配置） 和 `ssoProvider`（负责用户和token行为），进行初始化。所以这是一个可选接口，不必手动调用，如果强制调用，会覆盖掉默认行为，调用方法为。

```javascript
app = moka.setup([
	(dvaApp, $http) => {},
	...
]);
```

#### app = moka.use(plugin)
此接口为其实包装dva的一个插件的扩展接口，具体可以参考 [dva.use](https://github.com/dvajs/dva/blob/master/docs/API.md#appusehooks)

#### app = moka.modules(modules)
此接口封装了动态代码分割、延迟加载和路由加载、model注册功能。约定了模块的定义。

```javascript
app = moka.module([
  {
  	route: '/users',
  	module: () => import('./user'),
  },
  {
  	route: 'users/:userId',
  	module: () => import('./user/edit'),
  },
]);
```
> 注意，模块导入的 ./user/index.js 等应该符合模块入口文件的定义和规范

#### app.start(selector)
应用程序启动，程序装载及页面渲染，对应 dva 的 [app.start](https://github.com/dvajs/dva/blob/master/docs/API.md#appstartselector)


## 其他扩展接口
#### history.mapRouteToAction
此接口是在 dva 的 subscriptions 的范围内扩展出来的接口，方便用户编写页面订阅事件

```javascript
...
subscriptions: {
	setup({ history }) {
		return history.mapRouteToAction([
			{ path: '/users', action: 'userPage' },
			{ path: 'users/:userId', action: 'userEditPage' },
		]);
	}
},

effects: {
	*userPage({ payload: { params, query }, { call, put }) {
		// ...
	}
}
...
```

## 构建
构建的脚本和使用约定和原来其实是保持一致的，只不过，从这个版本开始，应用的构建部分，其实也是封装抽象到 moka 内维护的。@lattebank/moka 包含一个二进制可执行的NodeJS脚本，名字叫 coffeemill 的脚本。你会发现，原来的很多 babel 配置的依赖都不见了，项目根目录了没有了 gulpfile, 也没有依赖 @lattebank/build，这些其实都迁移到了 coffeemill 内部进行维护。现在唯一需要做的事情就是：

```javascript
"scripts": {
	"dll": "NODE_ENV=production coffeemill dll",
	"dev": "NODE_ENV=development coffeemill dev",
	"build": "NODE_ENV=production coffeemill build",
	"analyze": "ANALYZE=1 coffeemill analyze"
},
```

## demo 演示项目
请参考 

http://172.0.10.182/server-operation/tp320

请注意有一个v1分支，使用的 antd 和 react 的版本是不一样的，仔细查看一下区别， 

http://172.0.10.182/server-operation/tp320/tree/v1

具体演示场景，后面会补充， TODO


## 版本和发布管理

#### moka 版本定义
moka 是新版的摩卡应用框架，但是目前其本身是包含两个版本的（后期可能会废弃一个版本）

- 版本1， @lattebank/moka@1.x  其中 antd 为 `2.12.6`，react 为 `15.4.2`
- 版本2， @lattebank/moka@2.x  其中 antd 为 `3.3.3`， react 为 `16.2.0`

> 如果需要查看版本变更历史和详情，请查看 [Release Notes](http://172.0.10.182/client-web/moka/tags)

#### webadmin 模板版本定义

- 默认的 #developer 分支，对应原来的 webadmin 模板，saga 方案的摩卡应用
- 分支 #moka 分支，对应的 moka 应用框架，基于 @lattebank/moka@2.0.x
- 分支 #moka-pre 分支，对应的 moka 应用框架，基于 @lattebank/moka@1.0.x

> 一般使用类似于 `"@lattebank/moka": "~1.0.x"` 的方式定义依赖，这样的依赖版本约定，假设moka版本 a.b.c，小版本升c表示完全兼容的更改，可以是improvement或者是bugfix，应用会自动拉取版本的变更，中版本b的变更表示接口可能存在不兼容的变更，应用可以进行选择性的手动的版本升级。大版本a则是完全不兼容，老应用一般不考虑

#### tp320 演示项目版本定义

- develop 分支对应的是 @lattebank/moka@2.x 的演示应用
- v1 分支对应的是 @lattebank/moka@1.x 的演示应用


#### 版本更新指南，请务必遵守
- 在对应的版本分支更新代码
- 本地开发，更新和测试验证
- 提交代码
- 更新 `package.json` 里面的version，根据更改内容考虑更新小版本还是中版本
- git 打 tag
- 将代码和tag一起push到 gitlab 服务器上
- 更新 Release Note
- `rm -rf node_modules && npm i && npm run build`
- `npm publish` 到 npm 私有仓库中