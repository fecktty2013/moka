import eslint from 'rollup-plugin-eslint';
import commonjs from 'rollup-plugin-commonjs';
import resolve from 'rollup-plugin-node-resolve';
import babel from 'rollup-plugin-babel';

const pkg = require('./package.json');

const common = {
  external: [
    ...Object.keys(pkg.dependencies),
    ...Object.keys(pkg.peerDependencies),
    'dva-react-router-3/router',
    'antd/lib/message',
    'antd/lib/notification',
    'antd/lib/locale-provider',
    'antd/lib/locale-provider/zh_CN'
  ],
  plugins: [
    eslint(),
    commonjs({
      sourceMap: false
    }),
    resolve({
      browser: true,
      extensions: ['.js', '.json', '.jsx']
    }),
    babel({
      exclude: 'node_modules/**'
    }),
  ],
}

export default [{
    input: 'src/index.jsx',
    ...common,
    output: {
      file: 'lib/index.js',
      format: 'cjs'
    }
  },
  {
    input: 'src/plugins/bridge/index.jsx',
    ...common,
    output: {
      file: 'plugin/bridge.js',
      format: 'cjs'
    }
  }
];