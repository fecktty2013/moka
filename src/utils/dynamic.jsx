/* eslint-disable no-param-reassign */

import * as React from 'react';
import queryString from 'query-string';
import setupSubscription from './matchRoute';

function enhanceModel(model) {
  const enhancedModel = model;
  const originalSubscriptions = model.subscriptions;
  const overrideSubscriptions = {};
  if (originalSubscriptions != null) {
    Object.keys(originalSubscriptions).forEach((k) => {
      overrideSubscriptions[k] = function ({ dispatch, history }, done) {
        history.mapRouteToAction = function (mappings) {
          const resolvedMappings = mappings.constructor === Array ? mappings : [mappings];
          return history.listen(({ pathname, search }) => {
            const query = queryString.parse(search);
            resolvedMappings.forEach((mapping) => {
              setupSubscription(dispatch, mapping.path, pathname, query, mapping.action);
            });
          });
        };
        return originalSubscriptions[k].call(null, { dispatch, history }, done);
      };
    });
  }
  enhancedModel.subscriptions = overrideSubscriptions;
  return enhancedModel;
}

const cached = {};
export function registerModel(app, model) {
  model = model.default || model;
  if (!cached[model.namespace]) {
    const enhancedModel = enhanceModel(model);
    app.model(enhancedModel);
    cached[model.namespace] = 1;
  }
}

let defaultLoadingComponent = () => null;

function asyncComponent(config) {
  const { resolve } = config;

  return class DynamicComponent extends React.Component {
    constructor(...args) {
      super(...args);
      this.LoadingComponent =
        config.LoadingComponent || defaultLoadingComponent;
      this.state = {
        AsyncComponent: null,
      };
      this.load();
    }

    componentDidMount() {
      this.mounted = true;
    }

    componentWillUnmount() {
      this.mounted = false;
    }

    load() {
      resolve().then((m) => {
        const AsyncComponent = m.default || m;
        if (this.mounted) {
          this.setState({ AsyncComponent });
        } else {
          this.state.AsyncComponent = AsyncComponent; // eslint-disable-line
        }
      });
    }

    render() {
      const { AsyncComponent } = this.state;
      const { LoadingComponent } = this;
      if (AsyncComponent) return <AsyncComponent {...this.props} />;

      return <LoadingComponent {...this.props} />;
    }
  };
}

function internalDynamic(config) {  // eslint-disable-line
  const { app, models: resolveModels, component: resolveComponent } = config;
  return asyncComponent({
    resolve: config.resolve || function () {
      const models = typeof resolveModels === 'function' ? resolveModels() : [];
      const component = resolveComponent();
      return new Promise((resolve) => {
        Promise.all([...models, component]).then((ret) => {
          if (!models || !models.length) {
            return resolve(ret[0]);
          } else {
            const len = models.length;
            ret.slice(0, len).forEach((m) => {
              m = m.default || m;
              if (!Array.isArray(m)) {
                m = [m];
              }
              m.map(_ => registerModel(app, _));
            });
            return resolve(ret[len]);
          }
        });
      });
    },
    ...config,
  });
}

function internalDynamicForRoute3(config) {
  const { app, models: resolveModels, component: resolveComponent } = config;
  const resolveHandler = config.resolve || function () {
    const models = typeof resolveModels === 'function' ? resolveModels() : [];
    const component = resolveComponent();
    return new Promise((resolve) => {
      Promise.all([...models, component]).then((ret) => {
        if (!models || !models.length) {
          return resolve(ret[0]);
        } else {
          const len = models.length;
          ret.slice(0, len).forEach((m) => {
            m = m.default || m;
            if (!Array.isArray(m)) {
              m = [m];
            }
            m.map(_ => registerModel(app, _));
          });
          return resolve(ret[len]);
        }
      });
    });
  };
  return resolveHandler();
}

export default function dynamic(config) {
  const { app, module: resolveModule } = config;
  return internalDynamicForRoute3({
    resolve() {
      return resolveModule().then((ret) => {
        ret = ret.default || ret;
        const models = ret.models || [];
        if (ret.model) {
          models.push(ret.model);
        }
        models.map(_ => registerModel(app, _));
        return ret.component;
      });
    },
    ...config,
  });
}

dynamic.setDefaultLoadingComponent = (LoadingComponent) => {
  defaultLoadingComponent = LoadingComponent;
};

/* eslint-enable no-param-reassign */
