import * as React from 'react';
import notification from 'antd/lib/notification';

export default function ({ $http }) {
  $http.interceptors.response.use(
    null,
    function (error) { // eslint-disable-line
      const { code, message: msg, status } = error;
      if (status === 401) {
        if (code === 8063) {
          notification.destroy();
          notification.error({
            duration: 0,
            message: msg,
            description: <a href={`${head.CONTEXT_PATH}login?returnUrl=${encodeURIComponent(window.location.href)}`}>请重新登录</a>,
          });
        }
      } else if (status === 403) {
        if (code === 8065) {
          notification.destroy();
          notification.error({
            duration: 0,
            message: msg,
            description: '请确认你所属的角色',
          });
        }
      } else if (status === 422 || status === 500) {
        if (code === -1) {
          notification.destroy();
          notification.error({
            duration: 0,
            message: msg,
            description: '请刷新页面重试',
          });
        }
      }

      return Promise.reject(error);
    },
  );
}
