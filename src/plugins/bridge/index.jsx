import * as React from 'react';
import invariant from 'invariant';

const BRIDGESET = '@@moka/BRIDGE_SET';
const NAMESPACE = '@@bridge';

class LocalCache {
  constructor() {
    this.internalCache = {};
  }

  setCache(key, value) {
    this.internalCache[key] = value;
    if (this.notificationCallback) {
      setTimeout(() => {
        this.notificationCallback(this.internalCache);
      }, 0);
    }
  }

  init(props) {
    this.internalCache = props;
  }

  setNotificationCallback(notificationCallback) {
    this.notificationCallback = notificationCallback;
  }

  getCache(key) {
    return this.internalCache[key];
  }

  getAll() {
    return this.internalCache;
  }
}

const localCache = new LocalCache();

function createBridge(opts = {}) {
  const namespace = opts.namespace || NAMESPACE;

  const initialState = opts.initialState || {};

  localCache.init(initialState);

  const extraReducers = {
    [namespace](state = initialState, { type, payload }) {
      const { key, value } = payload || {};
      let ret;
      switch (type) {
        case BRIDGESET:
          ret = {
            ...state,
            [key]: value,
          };
          break;
        default:
          ret = state;
          break;
      }
      return ret;
    },
  };

  return {
    extraReducers,
  };
}

function bridgeSet(key, value) {
  localCache.setCache(key, value);
  return {
    type: BRIDGESET,
    payload: {
      key,
      value,
    },
  };
}

function withBridge(WrappedComponent) {
  return class BridgeComponent extends React.Component {
    constructor(props) {
      super(props);
      this.state = { bridge: localCache.getAll() };
    }

    componentDidMount() {
      localCache.setNotificationCallback((cache) => {
        this.setState({ bridge: cache });
      });
    }

    render() {
      const ownKeys = Object.keys(this.props);
      const bridgeKeys = Object.keys(this.state.bridge);
      invariant(!ownKeys.some(ok => bridgeKeys.some(bk => bk === ok)), 'Bridge keys are duplicated with prop keys of component');
      const newProps = { ...this.props, ...this.state.bridge };
      return <WrappedComponent {...newProps} />;
    }
  };
}

export {
  bridgeSet,
  withBridge,
};

export default createBridge;
