/* eslint-disable camelcase */
import * as React from 'react';
import dva, { connect } from 'dva-react-router-3';
import message from 'antd/lib/message';
import LocaleProvider from 'antd/lib/locale-provider';
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import { Router, Route, IndexRedirect } from 'dva-react-router-3/router';
import $http from '@lattebank/webadmin-http';
import dynamic, { registerModel } from './utils/dynamic';

import httpProvider from './providers/httpProvider';
import ssoProvider from './providers/ssoProvider';

let app;
let appOpts;

const defaultSetupProviders = [httpProvider, ssoProvider];
let localSetupProviders;

// opts: { defaultRoute, defaultModule ... }
const moka = function (opts) {
  appOpts = opts;
  app = dva({
    onError(error) {
      if (error.message) {
        message.destroy();
        message.error(error.message);
      }
    },
    ...opts,
  });
  if (opts.defaultModule && opts.defaultModule.model) {
    registerModel(app, opts.defaultModule.model);
  }
  return moka;
};

moka.setup = function (...setupProviders) {
  localSetupProviders = setupProviders;
};

moka.use = function (plugin) {
  app.use(plugin);
  return moka;
};

function renderDynamicRoutes(localApp, parentKey, modules) {
  return modules.map((mokaModule) => {
    const lazyComponent = (nextState, cb) => {
      dynamic({
        app: localApp,
        module: mokaModule.module,
      }).then(component => cb(null, component));
    };
    return (
      <Route key={parentKey + mokaModule.route} path={mokaModule.route} getComponent={lazyComponent}>
        {mokaModule.children && renderDynamicRoutes(localApp, mokaModule.route, mokaModule.children)}
      </Route>
    );
  });
}

// module: { route, module ... }
moka.modules = function (modules) {
  const dvaRoutes = function ({ history, app: localApp }) { // eslint-disable-line
    return (
      <LocaleProvider locale={zh_CN}>
        <Router history={history}>
          <Route path="/" component={appOpts.defaultModule && appOpts.defaultModule.component}>
            {appOpts.defaultRoute && <IndexRedirect to={appOpts.defaultRoute || '/'} />}
            {renderDynamicRoutes(localApp, 'root', modules)}
          </Route>
        </Router>
      </LocaleProvider>
    );
  };
  app.router(dvaRoutes);
  return moka;
};

moka.start = function (selector) {
  localSetupProviders = localSetupProviders || defaultSetupProviders;
  localSetupProviders.forEach(provider => provider({ app, $http }));
  return app.start(selector);
};

export {
  $http,
  connect,
};

export default moka;
/* eslint-enable camelcase */
