#!/usr/bin/env node

const webpack = require('webpack');
const gulp = require('gulp');
const runSequence = require('run-sequence');
const WebpackDevServer = require('webpack-dev-server')
const program = require('commander');
const pkg = require('../package.json');
const dllConfig = () => require('@lattebank/build/webpack.config.dll');
const prodConfig = () => require('@lattebank/build/webpack.config.prod');
const happyConfig = () => require('@lattebank/build/webpack.happy.prod');
const devConfig = () => require('@lattebank/build/webpack.config.dev');

require('./gulpfile');

const gulpTasks = {
  clean() {
    gulp.start(['_clean']);
  },
  copy() {
    gulp.start(['_copy']);
  },
  replace() {
    gulp.start(['_replace']);
  },
  check() {
    gulp.start(['_check']);
  },
  default() {
    gulp.start(['default']);
  },
}

const handleErrors = done => (err, stats) => {
  if (err) {
    console.error(err.stack || err);
    if (err.details) {
      console.error(err.details);
    }
    return done(err);
  }
  const info = stats.toJson();
  if (stats.hasErrors()) {
    console.error(info.errors);
    return done(err);
  }
  if (stats.hasWarnings()) {
    console.warn(info.warnings);
  }
  return done();
};

function buildCompiler(config) {
  return webpack(Object.assign(config, {
    stats: {
      assets: true,
      colors: true,
      errors: true,
      errorDetails: true,
      hash: true,
    }
  }));
}

const tasks = {
  dll() {
    process.env.NODE_ENV = 'production';
    const compiler = buildCompiler(dllConfig());
    compiler.run(handleErrors);
  },

  prod(done) {
    process.env.NODE_ENV = 'production';
    const compiler = buildCompiler(prodConfig());
    compiler.run(handleErrors(done));
  },

  happy(done) {
    process.env.NODE_ENV = 'production';
    const compiler = buildCompiler(happyConfig());
    compiler.run(handleErrors(done));
  },

  dev() {
    process.env.NODE_ENV = 'development';
    const builtDevConfig = devConfig();
    runSequence('default', function () {
      const compiler = buildCompiler(builtDevConfig);
      const server = new WebpackDevServer(compiler, {
        open: false,
        publicPath: '/static/js',
        contentBase: builtDevConfig.devServer.contentBase,
        historyApiFallback: builtDevConfig.devServer.historyApiFallback,
        hot: false,
        stats: builtDevConfig.devServer.stats,
        headers: builtDevConfig.devServer.headers
      });
      server.listen(builtDevConfig.devServer.port, builtDevConfig.devServer.host);
    });
  },

  build() {
    const self = this;
    runSequence('default', '_check', function () {
      self.happy(function () {
        gulpTasks.replace();
      });
    });
  },

  analyze() {
    process.env.ANALYZE = 1;
    this.prod(() => null);
  },
};

function runCommand(command) {
  switch(command) {
    case 'dll':
      tasks.dll();
      break;
    case 'dev':
      tasks.dev();
      break;
    case 'build':
      tasks.build();
      break;
    case 'analyze':
      tasks.analyze();
      break;
    default:
      throw new Error('Invalid command');
  }
}

program
  .version(pkg.version)
  .arguments('<command>')
  .action(runCommand)
  .parse(process.argv);
